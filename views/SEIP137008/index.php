<?php
/**
 *
 *
 * Date: 10/31/2016
 * Time: 10:03 AM
 */
use App\Person;
use App\Student;

function __autoload($ClassName){
    echo "<b>".$ClassName."</b>"."<br>";

    list($ns,$cn)=explode("\\",$ClassName);

    require_once("../../src/BITM/SEIP137008/".$cn.".php");
}



$objPerson = new Person();
echo $objPerson->showPersonInfo();

$objStudent = new Student();
echo $objStudent->showStudentInfo();


