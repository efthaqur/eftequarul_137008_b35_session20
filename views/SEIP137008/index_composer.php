<?php
/**
 * Date: 10/31/2016
 * Time: 12:45 PM
 */
require_once("../../vendor/autoload.php");

use App\Person;
use App\Student;

$objPerson = new Person();
echo $objPerson->showPersonInfo();

$objStudent = new Student();
echo $objStudent->showStudentInfo();

$newstu = new \App\NewStudent();
echo $newstu->showAdmissiondate();
